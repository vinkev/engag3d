Engag3ed Test - Kevin
=====================

Server side is powered by Laravel 5.5.x

## Server Requirements

* PHP >= 7.0
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

## Database Requirement
* Sqlite

# Step to Execute
* Rename `.env.example` into `.env` 
* Inside .env, change the value of DB_DATABASE to where the project will be located on the server.
```
DB_DATABASE=/{{path_to_project}}/engag3d_test/database/database.sqlite
```
* Run `composer install`
* Run `php artisan serve`
* Using postman link below, test the API

## Postman
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/27f85f320c9553b9dd78#?env%5Bengag3d%20-%20test%5D=W3siZW5hYmxlZCI6dHJ1ZSwia2V5IjoiaG9zdCIsInZhbHVlIjoiaHR0cDovLzEyNy4wLjAuMTo4MDAwIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6Im1vdmllX2lkIiwidmFsdWUiOiIxIiwidHlwZSI6InRleHQifV0=)

## API Documentation
[http://{{host}}/docs/index.html](http://{{host}}/docs/index.html)
