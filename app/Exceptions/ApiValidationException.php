<?php

namespace App\Exceptions;

use Illuminate\Validation\ValidationException;

class ApiValidationException extends ValidationException
{
    public function errors(){

        $transformed = [];

        foreach ($this->validator->errors()->messages() as $field => $message) {
            $transformed[] = [
                'location' => 'params',
                'param' => $field,
                'message' => $message[0],
            ];
        }

        return $transformed;
    }
}