<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Model\Movie
 *
 * @property int $id
 * @property string $genre
 * @property string $movie_name
 * @property string $rating
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereGenre($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereMovieName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Movie whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Movie extends Model
{
    use SoftDeletes;

    protected $table = 'movies';

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
