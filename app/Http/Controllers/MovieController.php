<?php

namespace App\Http\Controllers;

use App\Helpers\Classes\RestResponse;
use App\Http\Requests\MovieStoreRequest;
use App\Http\Requests\MovieUpdateRequest;
use App\Http\Resources\MovieResource;
use App\Model\Movie;
use Exception;
use Illuminate\Http\Request;
use Log;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();

        return MovieResource::collection($movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MovieStoreRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MovieStoreRequest $request)
    {
        try {
            $request->persist();
        } catch (Exception $e) {
            Log::error('Error on storing movie', [
                'exception'      => $e->getMessage(),
                'exception_file' => $e->getFile() . ':' . $e->getLine(),
                'trace'          => $e->getTraceAsString(),
                'request_data'   => $request->all()
            ]);

            return RestResponse::createResponseInternalServerError('Error on storing movie');
        }

        return RestResponse::createResponseCreated();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return MovieResource|\Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $movie = Movie::find($id);

        if (empty($movie)) {
            return RestResponse::createResponseNotFound('The specified movie could not be found');
        }

        return new MovieResource($movie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MovieUpdateRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MovieUpdateRequest $request, $id)
    {
        $movie = Movie::find($id);

        if (empty($movie)) {
            return RestResponse::createResponseNotFound('The specified movie could not be found');
        }

        try {
            $request->persist($movie);
        } catch (Exception $e) {
            Log::error('Error on updating movie', [
                'exception'      => $e->getMessage(),
                'exception_file' => $e->getFile() . ':' . $e->getLine(),
                'trace'          => $e->getTraceAsString(),
                'movie_id'       => $id,
                'request_data'   => $request->all()
            ]);

            return RestResponse::createResponseInternalServerError('Error on updating movie');
        }

        return RestResponse::createResponseNoContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);

        if (empty($movie)) {
            return RestResponse::createResponseNotFound('The specified movie could not be found');
        }

        try {
            $movie->delete();
        } catch (Exception $e) {
            Log::error('Error on deleting movie', [
                'exception'      => $e->getMessage(),
                'exception_file' => $e->getFile() . ':' . $e->getLine(),
                'trace'          => $e->getTraceAsString(),
                'movie_id'       => $id
            ]);

            return RestResponse::createResponseInternalServerError('Error on deleting movie');
        }

        return RestResponse::createResponseNoContent();
    }
}
