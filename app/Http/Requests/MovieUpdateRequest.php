<?php

namespace App\Http\Requests;

use App\Helpers\Constants\Genre;
use App\Helpers\Constants\Rating;
use App\Model\Movie;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MovieUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|string|max:100',
            'genre'  => ['required', Rule::in(Genre::getConstants())],
            'rating' => ['required', Rule::in(Rating::getConstants())]
        ];
    }

    /**
     * @param Movie $movie
     */
    public function persist(Movie $movie)
    {
        $movie->movie_name = $this->get('name');
        $movie->genre      = $this->get('genre');
        $movie->rating     = $this->get('rating');
        $movie->save();
    }
}
