<?php

namespace App\Helpers\Constants;

use App\Helpers\Abstracts\Constant;

class Genre extends Constant
{
    const ACTION   = 'action';
    const COMEDY   = 'comedy';
    const ROMANCE  = 'romance';
    const THRILLER = 'thriller';
}