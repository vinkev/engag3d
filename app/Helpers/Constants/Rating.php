<?php

namespace App\Helpers\Constants;

use App\Helpers\Abstracts\Constant;

class Rating extends Constant
{
    const GENERAL_AUDIENCE     = 'G';
    const PARENTAL_GUIDANCE    = 'PG';
    const PARENTAL_GUIDANCE_13 = 'PG-13';
    const RESTRICTED           = 'R';
    const ADULTS_ONLY          = 'NC-17';
}