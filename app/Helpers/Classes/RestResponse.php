<?php


namespace App\Helpers\Classes;


use Illuminate\Http\JsonResponse;
use stdClass;

class RestResponse
{
    public $details;


    private function successEmpty()
    {
        unset($this->details);

        return $this;
    }

    private function error($errors)
    {
        if ($errors) {
            $this->details = $errors;
        } else {
            $this->details = new stdClass();
        }

        return $this;
    }

    private function exception($errors = null)
    {
        if (empty($errors)) {
            unset($this->details);
        } else {
            if (is_string($errors)) {
                $this->details = [
                    'message' => $errors
                ];
            } else {
                $this->details = $errors;
            }
        }

        return $this;
    }

    /**
     * Response 201
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseCreated()
    {
        $response = new RestResponse();

        return response()->json(
            $response->successEmpty()
            , JsonResponse::HTTP_CREATED
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 204
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseNoContent()
    {
        $response = new RestResponse();

        return response()->json(
            $response->successEmpty()
            , JsonResponse::HTTP_NO_CONTENT
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 400
     *
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseBadRequest($errors = null)
    {
        $response = new RestResponse();

        return response()->json(
            $response->error($errors)
            , JsonResponse::HTTP_BAD_REQUEST
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 404
     *
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseNotFound($errors = null) {

        $response = new RestResponse();

        return response()->json(
            $response->error($errors)
            , JsonResponse::HTTP_NOT_FOUND
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 422
     *
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseUnprocessableEntity($errors = null)
    {
        $response = new RestResponse();

        return response()->json(
            $response->error($errors)
            , JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }

    /**
     * Response 500
     *
     * @param null $errors
     * @return \Illuminate\Http\JsonResponse
     */
    public static function createResponseInternalServerError($errors = null)
    {
        $response = new RestResponse();

        return response()->json(
            $response->exception($errors)
            , JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            , array('Content-Type' => 'application/json; charset=utf-8')
            , JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
        );
    }
}