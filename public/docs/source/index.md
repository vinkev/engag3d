---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_5caa7206219cb3641b1ee22ba93ea5e8 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://localhost/api/movie" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/movie",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[
    {
        "id": 1,
        "name": "Thor Ragnarok",
        "rating": "PG",
        "genre": "action"
    },
    {
        "id": 2,
        "name": "Justice League test",
        "rating": "PG",
        "genre": "action"
    }
]
```

### HTTP Request
`GET api/movie`

`HEAD api/movie`


<!-- END_5caa7206219cb3641b1ee22ba93ea5e8 -->

<!-- START_c929c95d3d689c4e4c43ce4b1bcbff80 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://localhost/api/movie" \
-H "Accept: application/json" \
    -d "name"="fugit" \
    -d "genre"="action" \
    -d "rating"="G" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/movie",
    "method": "POST",
    "data": {
        "name": "fugit",
        "genre": "action",
        "rating": "G"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/movie`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `100`
    genre | string |  required  | `action`, `comedy`, `romance` or `thriller`
    rating | string |  required  | `G`, `PG`, `PG-13`, `R` or `NC-17`

<!-- END_c929c95d3d689c4e4c43ce4b1bcbff80 -->

<!-- START_c8f4d25bd04260bf5a372dac6a4ae8d9 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://localhost/api/movie/{movie}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/movie/{movie}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 1,
    "name": "Thor Ragnarok",
    "rating": "PG",
    "genre": "action"
}
```

### HTTP Request
`GET api/movie/{movie}`

`HEAD api/movie/{movie}`


<!-- END_c8f4d25bd04260bf5a372dac6a4ae8d9 -->

<!-- START_237e0430692cf69e2ecfc8884aad090e -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://localhost/api/movie/{movie}" \
-H "Accept: application/json" \
    -d "name"="aut" \
    -d "genre"="action" \
    -d "rating"="G" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/movie/{movie}",
    "method": "PUT",
    "data": {
        "name": "aut",
        "genre": "action",
        "rating": "G"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/movie/{movie}`

`PATCH api/movie/{movie}`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | Maximum: `100`
    genre | string |  required  | `action`, `comedy`, `romance` or `thriller`
    rating | string |  required  | `G`, `PG`, `PG-13`, `R` or `NC-17`

<!-- END_237e0430692cf69e2ecfc8884aad090e -->

<!-- START_815d9f1849d0295f089c16cac137da2b -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/api/movie/{movie}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/movie/{movie}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/movie/{movie}`


<!-- END_815d9f1849d0295f089c16cac137da2b -->

